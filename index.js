let {
  showHorses,
  showAccount,
  setWager,
  clearRates,
  startRacing,
  newGame,
} = (() => {
  function Horse(name) {
    this.name = name;
    this.rate = 0;
    this.run = function () {
      return new Promise((resolve, reject) => {
        setTimeout(() => resolve(this), Math.random() * (3000 - 500) + 500);
      });
    };
  }
  let horses = [];
  let account = 100;

  return {
    showHorses: function () {
      horses.forEach((horse) => {
        console.log(horse);
      });
    },
    showAccount: function () {
      console.log(account);
    },
    setWager: function (name, amount) {
      let horse = horses.find((horse) => horse.name === name);
      if (horse.length === 0) {
        return new Error('Такой лошади не существует');
      }
      if (account < amount) {
        console.log('У вас недостаточно средств для ставки!');
      } else {
        horse.rate = amount;
        account -= amount;
      }
      // if (!namesArray.includes(name)) {
      // }
    },
    clearRates: function () {
      horses.forEach((horse) => {
        horse.rate = 0;
      });
    },
    startRacing: function () {
      let trophy = 0;
      const race = horses.map((horse) => horse.run());
      Promise.race(race).then((winner) => {
        console.log(`Победила ${winner.name}`);
        trophy = winner.rate * 2;
        account += trophy;
      });
      Promise.all(race).then(() => {
        if (trophy !== 0) {
          console.log(`Вы выиграли ${trophy}!`);
        }
        console.log(`Ваш текущий счёт ${account}`);
        clearRates();
      });
    },

    newGame: function () {
      horses = [];
      account = 100;
      horses.push(new Horse('Кейси'));
      horses.push(new Horse('Джонни'));
      horses.push(new Horse('Купер'));
    },
  };
})();

// const game = () => {
//   let horses = ['Кейси', 'Джонни', 'Купер'];
//   let account = 100;
//   const interface = () => {
//     function showHorses() {
//       horses.forEach((horse) => console.log(horse));
//     }
//   };
//   return interface;
// function run(horseName) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => resolve(horseName), Math.random() * (3000 - 500) + 500);
//   });
// }
// function clearRates() {
//   for (horse in this.horses) {
//     horse.value = 0;
//   }
// }
// addHorse: function Horse(name) {
//   this.name = name;
//   this.run = function () {
//     return new Promise((resolve, reject) => {
//       setTimeout(
//         () => resolve(this.name),
//         Math.random() * (3000 - 500) + 500
//       );
//     });
//   };
// },
// };
// function showHorses() {
//   for (horse in game.horses) {
//     console.log(horse);
//   }
// }
// function showAccount() {
//   console.log(game.account);
// }
// function setWager(name, amount) {
//   if (game.account < amount) {
//     console.log('У вас недостаточно средств для ставки!');
//   } else {
//     game.horses[name] = amount;
//     game.account -= amount;
//   }
// }
// function startRacing() {
//   let trophy = 0;
//   let currentRace = [];
//   for (horse in game.horses) {
//     currentRace.push(game.run(horse));
//   }
//   Promise.race(currentRace).then((value) => {
//     console.log(`Победила ${value}`);
//     trophy = game.horses[value] * 2;
//     game.account += trophy;
//   });
//   Promise.all(currentRace).then(() => {
//     if (trophy !== 0) {
//       console.log(`Вы выиграли ${trophy}!`);
//     }
//     console.log(`Ваш текущий счёт ${game.account}`);
//   });
//   game.clearRates();
// }

// function newGame() {
//   game.clearRates();
//   game.account = 100;
// }
newGame();
